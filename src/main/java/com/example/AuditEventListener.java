package com.example;

import com.atlassian.bitbucket.event.repository.RepositoryAccessedEvent;
import com.atlassian.bitbucket.event.repository.RepositoryOtherReadEvent;
import com.atlassian.bitbucket.event.repository.RepositoryPullEvent;
import com.atlassian.bitbucket.event.request.RequestEvent;
import com.atlassian.event.api.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Named;
import java.text.SimpleDateFormat;

@Named("AuditEventListener")
public class AuditEventListener {
    private static final Logger log = LoggerFactory.getLogger(AuditEventListener.class);


    @EventListener
    public void onRequestEvent(RequestEvent requestEvent) {
        log.info(requestEvent.getRequestContext().getRemoteAddress());
        log.info(requestEvent.getRequestContext().getDetails());
    }

    @EventListener
    public void onAccessEvent(RepositoryAccessedEvent accessedEvent) {
        log.info(accessedEvent.getUser() + " accessed " + accessedEvent.getRepository());
    }

    @EventListener
    public void onPull(RepositoryPullEvent pullEvent) {
        log.info(pullEvent.getUser() + " accessed " + pullEvent.getRepository());
    }

    @EventListener
    public void onRead(RepositoryOtherReadEvent readEvent) {
        log.info(readEvent.getUser() + " accessed " + readEvent.getRepository());
    }
}